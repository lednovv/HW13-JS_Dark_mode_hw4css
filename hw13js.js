let checkbox = document.getElementById("ChangeTheme");

if (localStorage.getItem("mode") == "dark") {
    darkMode();
} else {
    offDarkMode();
}


checkbox.addEventListener("change", function() {

    if (checkbox.checked) {
        darkMode();
    } else {
        offDarkMode();
    }
});


function darkMode() {
    document.body.classList.add("dark-mode");
    document.querySelector(".post-container").classList.add("dark-mode")
    document.querySelector(".top-section").classList.add("dark-mode")
    checkbox.checked = true;
    localStorage.setItem("mode", "dark");
}


function offDarkMode() {
    document.body.classList.remove("dark-mode");
    document.querySelector(".post-container").classList.remove("dark-mode")
    document.querySelector(".top-section").classList.remove("dark-mode")
    checkbox.checked = false;
    localStorage.setItem("mode", "light");
}
